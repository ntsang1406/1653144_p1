Họ tên: Nguyễn Tấn Sang
Lớp: 16CLC2
MSSV: 1653144

Nội dung đồ án: Hoàn thành tất cả các yêu cầu
- Khi user chọn menu item hay toolbar, thực hiện dúng yêu cầu: Hoàn thành
- Color và Font: Hoàn thành tất cả yêu cầu
- Draw: Hoàn thành đầy đủ 4 loại vẽ
- Paint lại các child window: Hoàn thành yêu cầu
- Edit object: Hoàn thành đầy đủ việc di chuyển object và chỉnh sửa kích thước, riêng Text chỉ có thể di chuyển object.
- Save file: Hoàn thành, save đúng và đủ, đúng yêu cầu, hiển thị tên file lên window
- Open: Mở được file, đúng yêu cầu, hiển thị tên file lên window
Bitbucket: https://bitbucket.org/ntsang1406/1653144_p1/src

Link video demo: https://www.youtube.com/watch?v=TiqEy7h4Stc
Em quay bằng phần mềm có sẵn trên Windows 10, nó không bắt được các hôp thoại được mở, và chuột bị lệch

Một số lưu ý:
- Nếu thầy compile code, xin thầy compile trên x86 thay vì x64
- Có vãi lỗi gây crash chương trình. Mong thầy đọc một số dưới đây để tránh một số trường hợp thường gặp.
- Color và Font chỉ mở được khi có ít nhất 1 cửa sổ con, không thì chương trình sẽ bị crash
- Chức năng vẽ Text sẽ tạo 1 edit box, người dùng nhập vào và click 1 lần nữa ra ngoài để text được hiển thị
- Chức năng select object sẽ select 1 object, click ra ngoài object đó để bỏ chọn. Chỉ chọn được cái ở trên cùng nhất.
- Em chưa thêm các dòng lệnh để người dùng bấm tùy ý khi đang làm giữa chừng việc nào đó,
ví dụ như đang gõ text trên edit box thì hãy click ra ngoài rồi hãy chọn nét vẽ khác, hoặc select object
thì phải click ra ngoài bỏ chọn mới chọn chức năng khác được, không sẽ bị crash. Vậy nên xin
thầy lúc chấm cẩn thận.
- Không rõ lỗi lúc mở file nếu thao tác mở quá nhanh sẽ gây crash, cần chờ 2 3 giây trước khi nhấn
Open sẽ không gây crash.
- Nếu thầy chấm bị lỗi xin thầy liên hệ email em là ntsang1406@gmail.com.

Source code được viết trên Visual Studio Community 2017