// 1653144_P1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1653144_P1.h"
#include <CommCtrl.h>
#include <commdlg.h>
#include "Object.h"
#include <fstream>
using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND hToolbarWnd, hMDIClientWnd, hFrameWnd, hActiveChildWnd;
UINT checkedDrawType = ID_DRAW_LINE;
UINT nChildWnd = 0;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK ChildWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK EditWndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY1653144P1, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1653144P1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateMDISysAccel(hMDIClientWnd, &msg) && !TranslateAccelerator(hFrameWnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1653144P1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MY1653144P1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
   hFrameWnd = hWnd;

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

VOID CreateToolbarWnd(HWND hWnd) {
	InitCommonControls();
	TBBUTTON toolbarButton[] = {
		{ 0, ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 3, ID_DRAW_LINE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 4, ID_DRAW_RECTANGLE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 5, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 6, ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 7, ID_DRAW_SELECT_OBJECT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 }
	};
	HBITMAP hBitmapIcon = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_ICON));
	hToolbarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(toolbarButton) / sizeof(TBBUTTON),
		0,
		(UINT)hBitmapIcon,
		toolbarButton,
		sizeof(toolbarButton) / sizeof(TBBUTTON),
		20,
		20,
		20,
		20,
		sizeof(TBBUTTON));
}

HWND CreateMDIClientWnd(HWND hWnd) {
	HWND hMDIClientWnd;
	CLIENTCREATESTRUCT  ccs;
	ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
	ccs.idFirstChild = 100001;
	hMDIClientWnd = CreateWindowEx(WS_EX_CLIENTEDGE,
		L"MDICLIENT",
		NULL,
		WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		hWnd,
		NULL,
		hInst,
		(LPVOID)&ccs);
	ShowWindow(hMDIClientWnd, SW_SHOW);
	return hMDIClientWnd;
}

UINT RecheckDrawMenu(HWND hWnd, UINT uncheckDrawType, UINT checkDrawType) {
	HMENU hDrawMenu;
	hDrawMenu = GetSubMenu(GetMenu(hWnd), 1);
	CheckMenuItem(hDrawMenu, uncheckDrawType, MF_BYCOMMAND | MF_UNCHECKED);
	CheckMenuItem(hDrawMenu, checkDrawType, MF_BYCOMMAND | MF_CHECKED);
	return checkDrawType;
}

ATOM RegisterChildWndClass() {
	WNDCLASSEXW wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = ChildWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = sizeof(PAINTDATA);
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_MY1653144P1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = 0;
	wcex.lpszClassName = L"MDICHILD_DRAW";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
	return RegisterClassExW(&wcex);
}

HWND CreateChildWnd(HWND hMDIClientWnd, UINT &nChildWnd) {
	WCHAR childWndTitle[100];
	MDICREATESTRUCT mcs;
	ZeroMemory(&mcs, sizeof(MDICREATESTRUCT));
	wsprintf(childWndTitle, L"No name - %d.drw", ++nChildWnd);
	return CreateWindowEx(WS_EX_MDICHILD,
		L"MDICHILD_DRAW",
		childWndTitle,
		WS_CHILD | WS_VISIBLE | WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		hMDIClientWnd,
		NULL,
		hInst,
		&mcs);
}

LRESULT CALLBACK CloseChildWnd(HWND hChildWnd, LPARAM lParam) {
	SendMessage(hChildWnd, WM_CLOSE, 0, 0);
	return 1;
}

VOID SelectColor(HWND hWnd) {
	CHOOSECOLOR cc;
	COLORREF acrCustClr[16];
	PAINTDATA *paintData = (PAINTDATA *)GetWindowLongPtr(hActiveChildWnd, 0);
	ZeroMemory(&cc, sizeof(CHOOSECOLOR));
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = paintData->color;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
	if (ChooseColor(&cc))
		paintData->color = cc.rgbResult;
}

VOID SelectFont(HWND hWnd) {
	CHOOSEFONT cf;
	HFONT hFont;
	PAINTDATA *paintData = (PAINTDATA *)GetWindowLongPtr(hActiveChildWnd, 0);
	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd;
	cf.lpLogFont = &paintData->font;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS | CF_INITTOLOGFONTSTRUCT;
	ChooseFont(&cf);
	/*if (ChooseFont(&cf)) {
		paintData->font = *cf.lpLogFont;
		hFont = CreateFontIndirect(cf.lpLogFont);
		GetObject(hFont, sizeof(LOGFONT), (LPVOID)&paintData->font);
	}*/
}

void OpenFile(HWND hWnd) {
	OPENFILENAME ofn;
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT("My-Paint Files (*.drw)\0*.drw\0");
	ifstream file;
	PAINTDATA *paintData;
	HWND hNewChildWnd;
	szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn)) {
		file.open(szFile, ios::binary);
		paintData = (PAINTDATA *)VirtualAlloc(NULL, sizeof(PAINTDATA), MEM_COMMIT, PAGE_READWRITE);
		paintData->Read(file);
		file.close();
		hNewChildWnd = CreateChildWnd(hMDIClientWnd, nChildWnd);
		SetWindowText(hNewChildWnd, szFile);
		PAINTDATA *oldPaintData = (PAINTDATA *)GetWindowLongPtr(hNewChildWnd, 0);
		memcpy(oldPaintData, paintData, sizeof(PAINTDATA));
		InvalidateRect(hNewChildWnd, NULL, TRUE);
	}
}

void SaveFile(HWND hWnd) {
	OPENFILENAME ofn;
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT("My-Paint Files (*.drw)\0*.drw\0");
	PAINTDATA *paintData = (PAINTDATA *)GetWindowLongPtr(hActiveChildWnd, 0);
	ofstream file;
	if (!paintData->saved) {
		szFile[0] = '\0';
		ZeroMemory(&ofn, sizeof(OPENFILENAME));
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = hWnd;
		ofn.lpstrFilter = szFilter;
		ofn.nFilterIndex = 1;
		ofn.lpstrFile = szFile;
		ofn.nMaxFile = sizeof(szFile);
		ofn.Flags = OFN_OVERWRITEPROMPT;
		ofn.lpstrDefExt = TEXT("drw");
		if (!GetSaveFileName(&ofn))
			return;
	}
	else
		GetWindowText(hActiveChildWnd, szFile, 256);
	file.open(szFile, ios::binary);
	paintData->Write(file);
	file.close();
	SetWindowText(hActiveChildWnd, szFile);
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HMENU hDrawMenu;
    switch (message)
    {
	case WM_CREATE:
		hDrawMenu = GetSubMenu(GetMenu(hWnd), 1);
		CheckMenuItem(hDrawMenu, ID_DRAW_LINE, MF_BYCOMMAND | MF_CHECKED);
		CreateToolbarWnd(hWnd);
		hMDIClientWnd = CreateMDIClientWnd(hWnd);
		RegisterChildWndClass();
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
			switch (wmId)
			{
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			case ID_FILE_NEW:
				CreateChildWnd(hMDIClientWnd, nChildWnd);
				break;
			case ID_FILE_OPEN:
				MessageBox(hWnd, L"Bạn đã chọn Open", L"Notification", MB_OK);
				OpenFile(hWnd);
				break;
			case ID_FILE_SAVE:
				MessageBox(hWnd, L"Bạn đã chọn Save", L"Notification", MB_OK);
				SaveFile(hWnd);
				break;
			case ID_DRAW_COLOR:
				SelectColor(hWnd);
				break;
			case ID_DRAW_FONT:
				SelectFont(hWnd);
				break;
			case ID_DRAW_LINE:
			case ID_DRAW_RECTANGLE:
			case ID_DRAW_ELLIPSE:
			case ID_DRAW_TEXT:
			case ID_DRAW_SELECT_OBJECT:
				checkedDrawType = RecheckDrawMenu(hWnd, checkedDrawType, wmId);
				break;
			case ID_WINDOW_TIDE:
				SendMessage(hMDIClientWnd, WM_MDITILE, MDITILE_SKIPDISABLED, 0);
				break;
			case ID_WINDOW_CASCADE:
				SendMessage(hMDIClientWnd, WM_MDICASCADE, MDITILE_SKIPDISABLED, 0);
				break;
			case ID_WINDOW_CLOSE_ALL:
				EnumChildWindows(hMDIClientWnd, (WNDENUMPROC)CloseChildWnd, 0);
				break;
			default:
				return DefFrameProc(hWnd, hMDIClientWnd, message, wParam, lParam);
			}
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
	case  WM_SIZE:
		UINT  nWndWidth, nWndHeight;
		nWndWidth = LOWORD(lParam);
		nWndHeight = HIWORD(lParam);
		MoveWindow(hMDIClientWnd,  0,  30, nWndWidth, nWndHeight - 30,  TRUE);
		break;
    default:
		return DefFrameProc(hWnd, hMDIClientWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void InitPaintData(HWND hWnd) {
	PAINTDATA *paintData;
	paintData = (PAINTDATA *)VirtualAlloc(NULL, sizeof(PAINTDATA), MEM_COMMIT, PAGE_READWRITE);
	paintData->color = RGB(0, 0, 0);
	ZeroMemory(&paintData->font, sizeof(LOGFONT));
	paintData->font.lfHeight = -16;
	wcscpy_s(paintData->font.lfFaceName, LF_FACESIZE, L"Arial");
	paintData->nObject = 0;
	SetWindowLongPtr(hWnd, 0, (LONG_PTR)paintData);
}

LRESULT CALLBACK ChildWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	static PAINTOBJECT *object = NULL;
	PAINTDATA *paintData = (PAINTDATA *)GetWindowLongPtr(hWnd, 0);
	static PAINTTEXT *textObject;
	HDC hdc;
	static HWND hEditWnd;
	static bool texting = FALSE, select = FALSE;
	static PAINTPOINT position;
	static SELECTRECTANGLE *selectRectangle;
	switch (message) {
	case WM_CREATE:
		InitPaintData(hWnd);
		break;
	case WM_MDIACTIVATE:
		hActiveChildWnd = hWnd;
		break;
	case WM_PAINT:
		PAINTSTRUCT ps;
		int i;
		hdc = BeginPaint(hWnd, &ps);
		for (i = 0; i < paintData->nObject; i++)
			paintData->object[i]->Draw(hdc);
		EndPaint(hWnd, &ps);
		break;
	case WM_LBUTTONDOWN:
		switch (checkedDrawType) {
		case ID_DRAW_LINE:
			object = new PAINTLINE();
			break;
		case ID_DRAW_ELLIPSE:
			object = new PAINTELLIPSE();
			break;
		case ID_DRAW_RECTANGLE:
			object = new PAINTRECTANGLE();
			break;
		}
		switch (checkedDrawType) {
		case ID_DRAW_LINE:
		case ID_DRAW_ELLIPSE:
		case ID_DRAW_RECTANGLE:
			object->start.x = object->end.x = LOWORD(lParam);
			object->start.y = object->end.y = HIWORD(lParam);
			object->color = paintData->color;
			paintData->object[paintData->nObject] = object;
			paintData->nObject++;
			break;
		case ID_DRAW_TEXT:
			if (!texting) {
				textObject = new PAINTTEXT();
				textObject->start.x = LOWORD(lParam);
				textObject->start.y = HIWORD(lParam);
				textObject->color = paintData->color;
				textObject->font = paintData->font;
				hEditWnd = CreateWindowEx(NULL,
					L"EDIT",
					NULL,
					WS_VISIBLE | WS_CHILD | WS_BORDER | ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL,
					textObject->start.x,
					textObject->start.y,
					300,
					abs(paintData->font.lfHeight * 1.0 / 72 * 96),
					hWnd,
					NULL,
					hInst,
					NULL);
				HFONT hFont = CreateFontIndirect(&paintData->font);
				SendMessage(hEditWnd, WM_SETFONT, (WPARAM)hFont, TRUE);
				SetFocus(hEditWnd);
				texting = TRUE;
			}
			else {
				GetWindowText(hEditWnd, textObject->text, GetWindowTextLength(hEditWnd) + 1);
				DestroyWindow(hEditWnd);
				paintData->object[paintData->nObject] = textObject;
				paintData->nObject++;
				InvalidateRect(hWnd, NULL, TRUE);
				texting = FALSE;
			}
			break;
		case ID_DRAW_SELECT_OBJECT:
			if (!select) {
				position.x = LOWORD(lParam);
				position.y = HIWORD(lParam);
				object = paintData->Select(position);
				if (object != NULL) {
					selectRectangle = new SELECTRECTANGLE(object);
					object = selectRectangle;
					paintData->object[paintData->nObject] = selectRectangle;
					paintData->nObject++;
					InvalidateRect(hWnd, NULL, TRUE);
					select = TRUE;
				}
			}
			else {
				position.x = LOWORD(lParam);
				position.y = HIWORD(lParam);
				if (!object->Select(position)) {
					selectRectangle->object->ChangePoint();
					selectRectangle->object = NULL;
					delete paintData->object[paintData->nObject - 1];
					paintData->nObject--;
					InvalidateRect(hWnd, NULL, TRUE);
					select = FALSE;
					object = NULL;
				}
			}
			break;
		}
		break;
	case WM_MOUSEMOVE:
		if (checkedDrawType != ID_DRAW_SELECT_OBJECT || object != NULL)
			if (wParam & MK_LBUTTON == MK_LBUTTON && checkedDrawType != ID_DRAW_TEXT) {
				switch (checkedDrawType) {
				case ID_DRAW_LINE:
				case ID_DRAW_ELLIPSE:
				case ID_DRAW_RECTANGLE:
					object->end.x = LOWORD(lParam);
					object->end.y = HIWORD(lParam);
					break;
				case ID_DRAW_TEXT:
				case ID_DRAW_SELECT_OBJECT:
					object->Move(LOWORD(lParam) - position.x, HIWORD(lParam) - position.y);
					position.x = LOWORD(lParam);
					position.y = HIWORD(lParam);
					break;
				}
				InvalidateRect(hWnd, NULL, TRUE);
			}
		break;
	case WM_LBUTTONUP:
		if (checkedDrawType != ID_DRAW_SELECT_OBJECT || object != NULL)
			if (checkedDrawType != ID_DRAW_TEXT) {
				InvalidateRect(hWnd, NULL, TRUE);
				if (checkedDrawType != ID_DRAW_SELECT_OBJECT)
					object->ChangePoint();
				break;
			}
		break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK EditWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	return DefWindowProc(hWnd, message, wParam, lParam);
}