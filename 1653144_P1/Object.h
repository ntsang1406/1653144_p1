#pragma once
#include <Windows.h>
#include <fstream>
using namespace std;

struct PAINTPOINT {
	int x, y;
};

class PAINTOBJECT {
public:
	COLORREF color;
	PAINTPOINT start, end;
	bool checkText = FALSE;
	bool checkLine = FALSE;
	virtual void ChangePoint() {
		PAINTPOINT newStart, newEnd;
		if (start.x < end.x) {
			newStart.x = start.x;
			newEnd.x = end.x;
		}
		else {
			newStart.x = end.x;
			newEnd.x = start.x;
		}
		if (start.y < end.y) {
			newStart.y = start.y;
			newEnd.y = end.y;
		}
		else {
			newStart.y = end.y;
			newEnd.y = start.y;
		}
		start = newStart;
		end = newEnd;
	}
	virtual void Draw(HDC hdc) = 0;
	virtual void Read(ifstream &file) {
		file.read((char *)&color, sizeof(COLORREF));
		file.read((char *)&start, sizeof(PAINTPOINT));
		file.read((char *)&end, sizeof(PAINTPOINT));
	}
	virtual void Write(ofstream &file) = 0;
	virtual bool Select(PAINTPOINT position) = 0;
	virtual void Move(int x, int y) {
		start.x += x;
		end.x += x;
		start.y += y;
		end.y += y;
	}
	virtual void MoveStart(int x, int y) {
		start.x += x;
		start.y += y;
	}
	virtual void MoveEnd(int x, int y) {
		end.x += x;
		end.y += y;
	}
};

class PAINTLINE : public PAINTOBJECT {
public:
	PAINTLINE() {
		checkLine = TRUE;
	}
	void ChangePoint() {
		return;
	}
	void Draw(HDC hdc);
	void Write(ofstream &file);
	bool Select(PAINTPOINT position);
};

class PAINTELLIPSE : public PAINTOBJECT {
public:
	void Draw(HDC hdc);
	void Write(ofstream &file);
	bool Select(PAINTPOINT position);
};

class PAINTRECTANGLE : public PAINTOBJECT {
public:
	void Draw(HDC hdc);
	void Write(ofstream &file);
	bool Select(PAINTPOINT position);
};

class PAINTTEXT : public PAINTOBJECT {
public:
	LOGFONT font;
	WCHAR text[100];
	PAINTTEXT() {
		checkText = TRUE;
	}
	void ChangePoint() {
		return;
	}
	void CalculateEnd(HDC hdc);
	void Draw(HDC hdc);
	void Read(ifstream &file);
	void Write(ofstream &file);
	bool Select(PAINTPOINT position);
};

class PAINTDATA {
public:
	COLORREF color;
	LOGFONT font;
	PAINTOBJECT *object[100];
	int nObject;
	bool saved = FALSE;
	void Read(ifstream &file);
	void Write(ofstream &file);
	PAINTOBJECT *Select(PAINTPOINT position);
};

class SELECTCIRCLE : public PAINTELLIPSE {
public:
	int type;
	PAINTPOINT center;
	PAINTRECTANGLE *selectRectangle;
	SELECTCIRCLE() {
		selectRectangle = NULL;
		return;
	}
	SELECTCIRCLE(PAINTPOINT center, int type, PAINTRECTANGLE *selectRectangle) {
		this->center = center;
		start.x = center.x - 5;
		start.y = center.y - 5;
		end.x = center.x + 5;
		end.y = center.y + 5;
		this->type = type;
		this->selectRectangle = selectRectangle;
	}
	void MoveStart(int x, int y) {
		PAINTELLIPSE::Move(x, y);
		center.x += x;
		center.y += y;
		selectRectangle->MoveStart(x, y);
	}
	void MoveEnd(int x, int y) {
		PAINTELLIPSE::Move(x, y);
		center.x += x;
		center.y += y;
		selectRectangle->MoveEnd(x, y);
	}
	void Move(int x, int y) {
		PAINTELLIPSE::Move(x, y);
		center.x += x;
		center.y += y;
	}
};

class SELECTRECTANGLE : public PAINTRECTANGLE {
public:
	PAINTOBJECT *object;
	SELECTCIRCLE *startCircle, *endCircle;
	int currentSelect = 0;
	SELECTRECTANGLE() {
		startCircle = NULL;
		endCircle = NULL;
		return;
	}
	SELECTRECTANGLE(PAINTOBJECT *object) {
		PAINTPOINT newStart, newEnd;
		this->object = object;
		color = RGB(128, 128, 128);
		if (!object->checkLine) {
			if (object->start.x < object->end.x) {
				newStart.x = object->start.x;
				newEnd.x = object->end.x;
			}
			else {
				newStart.x = object->end.x;
				newEnd.x = object->start.x;
			}
			if (object->start.y < object->end.y) {
				newStart.y = object->start.y;
				newEnd.y = object->end.y;
			}
			else {
				newStart.y = object->end.y;
				newEnd.y = object->start.y;
			}
			start = newStart;
			end = newEnd;
		}
		else {
			start = object->start;
			end = object->end;
		}
		if (!object->checkText) {
			startCircle = new SELECTCIRCLE(start, 0, this);
			endCircle = new SELECTCIRCLE(end, 1, this);
		}
	}
	void Move(int x, int y) {
		switch (currentSelect) {
		case 0:
			PAINTOBJECT::Move(x, y);
			object->Move(x, y);
			if (!object->checkText) {
				startCircle->Move(x, y);
				endCircle->Move(x, y);
			}
			break;
		case 1:
			startCircle->MoveStart(x, y);
			break;
		case 2:
			endCircle->MoveEnd(x, y);
			break;
		}
	}
	void Draw(HDC hdc) {
		PAINTRECTANGLE::Draw(hdc);
		if (!object->checkText) {
			startCircle->Draw(hdc);
			endCircle->Draw(hdc);
		}
	}
	void MoveStart(int x, int y) {
		PAINTOBJECT::MoveStart(x, y);
		object->MoveStart(x, y);
	}
	void MoveEnd(int x, int y) {
		PAINTOBJECT::MoveEnd(x, y);
		object->MoveEnd(x, y);
	}
	bool Select(PAINTPOINT position) {
		if (!object->checkText) {
			if (startCircle != NULL && startCircle->Select(position)) {
				currentSelect = 1;
				return TRUE;
			}
			if (endCircle != NULL && endCircle->Select(position)) {
				currentSelect = 2;
				return TRUE;
			}
		}
		if (PAINTRECTANGLE::Select(position)) {
			currentSelect = 0;
			return TRUE;
		}
		return FALSE;
	}
};