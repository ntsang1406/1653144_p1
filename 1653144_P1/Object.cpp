#include "stdafx.h"
#include "Object.h"

void PAINTLINE::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 2, color);
	SelectObject(hdc, hPen);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	MoveToEx(hdc, start.x, start.y, NULL);
	LineTo(hdc, end.x, end.y);
	DeleteObject(hPen);
}

void PAINTLINE::Write(ofstream &file) {
	int drawType = 0;
	file.write((char *)&drawType, sizeof(int));
	file.write((char *)&color, sizeof(COLORREF));
	file.write((char *)&start, sizeof(PAINTPOINT));
	file.write((char *)&end, sizeof(PAINTPOINT));
}

bool PAINTLINE::Select(PAINTPOINT position) {
	PAINTPOINT newStart, newEnd;
	double num, denom;
	num = (end.y - start.y) * position.x - (end.x - start.x) * position.y + end.x * start.y - end.y * start.x;
	num = abs(num);
	denom = sqrt((end.y - start.y) * (end.y - start.y) + (end.x - start.x) * (end.x - start.x));
	if (num / denom > 5)
		return FALSE;
	if (start.x < end.x) {
		newStart.x = start.x;
		newEnd.x = end.x;
	}
	else {
		newStart.x = end.x;
		newEnd.x = start.x;
	}
	if (start.y < end.y) {
		newStart.y = start.y;
		newEnd.y = end.y;
	}
	else {
		newStart.y = end.y;
		newEnd.y = start.y;
	}
	if (position.x < newStart.x || position.x > newEnd.x)
		return FALSE;
	if (position.y < newStart.y || position.y > newEnd.y)
		return FALSE;
	return TRUE;
}

void PAINTELLIPSE::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 2, color);
	SelectObject(hdc, hPen);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Ellipse(hdc, start.x, start.y, end.x, end.y);
	DeleteObject(hPen);
}

void PAINTELLIPSE::Write(ofstream &file) {
	int drawType = 1;
	file.write((char *)&drawType, sizeof(int));
	file.write((char *)&color, sizeof(COLORREF));
	file.write((char *)&start, sizeof(PAINTPOINT));
	file.write((char *)&end, sizeof(PAINTPOINT));
}

bool PAINTELLIPSE::Select(PAINTPOINT position) {
	double a, b, c, d1, d2, major;
	PAINTPOINT foci1, foci2;
	a = (end.x - start.x) / 2;
	b = (end.y - start.y) / 2;
	c =	sqrt(abs(a * a - b * b));
	if (a > b) {
		foci1.y = foci2.y = start.y + b;
		foci1.x = start.x + a - c;
		foci2.x = start.x + a + c;
		major = a;
	}
	else {
		foci1.x = foci2.x = start.x + a;
		foci1.y = start.y + b - c;
		foci2.y = start.y + b + c;
		major = b;
	}
	d1 = sqrt((position.x - foci1.x) * (position.x - foci1.x) + (position.y - foci1.y) * (position.y - foci1.y));
	d2 = sqrt((position.x - foci2.x) * (position.x - foci2.x) + (position.y - foci2.y) * (position.y - foci2.y));
	if (d1 + d2 <= 2 * major)
		return TRUE;
	return FALSE;
}

void PAINTRECTANGLE::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 2, color);
	SelectObject(hdc, hPen);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, start.x, start.y, end.x, end.y);
	DeleteObject(hPen);
}

void PAINTRECTANGLE::Write(ofstream &file) {
	int drawType = 2;
	file.write((char *)&drawType, sizeof(int));
	file.write((char *)&color, sizeof(COLORREF));
	file.write((char *)&start, sizeof(PAINTPOINT));
	file.write((char *)&end, sizeof(PAINTPOINT));
}

bool PAINTRECTANGLE::Select(PAINTPOINT position) {
	PAINTPOINT newStart, newEnd;
	if (start.x < end.x) {
		newStart.x = start.x;
		newEnd.x = end.x;
	}
	else {
		newStart.x = end.x;
		newEnd.x = start.x;
	}
	if (start.y < end.y) {
		newStart.y = start.y;
		newEnd.y = end.y;
	}
	else {
		newStart.y = end.y;
		newEnd.y = start.y;
	}
	/*if (start.x <= position.x && position.x <= end.x &&
		start.y <= position.y && position.y <= end.y)
		return true;*/
	if (newStart.x <= position.x && position.x <= newEnd.x &&
		newStart.y <= position.y && position.y <= newEnd.y)
		return true;
	return false;
}

void PAINTTEXT::CalculateEnd(HDC hdc) {
	SIZE sz;
	GetTextExtentPoint32(hdc, text, wcslen(text), &sz);
	end.x = start.x + sz.cx;
	end.y = start.y + sz.cy;
}

void PAINTTEXT::Draw(HDC hdc) {
	HFONT hFont;
	hFont = CreateFontIndirect(&font);
	SelectObject(hdc, hFont);
	SetTextColor(hdc, color);
	TextOut(hdc, start.x, start.y, text, wcslen(text));
	CalculateEnd(hdc);
	checkText = TRUE;
}

void PAINTTEXT::Read(ifstream &file) {
	file.read((char *)&color, sizeof(COLORREF));
	file.read((char *)&start, sizeof(PAINTPOINT));
	file.read((char *)&end, sizeof(PAINTPOINT));
	file.read((char *)&font, sizeof(LOGFONT));
	file.read((char *)text, 100 * sizeof(WCHAR));
	checkText = TRUE;
}

void PAINTTEXT::Write(ofstream &file) {
	int drawType = 3;
	file.write((char *)&drawType, sizeof(int));
	file.write((char *)&color, sizeof(COLORREF));
	file.write((char *)&start, sizeof(PAINTPOINT));
	file.write((char *)&end, sizeof(PAINTPOINT));
	file.write((char *)&font, sizeof(LOGFONT));
	file.write((char *)text, 100 * sizeof(WCHAR));
}

bool PAINTTEXT::Select(PAINTPOINT position) {
	if (start.x <= position.x && position.x <= end.x &&
		start.y <= position.y && position.y <= end.y)
		return true;
	return false;
}

void PAINTDATA::Read(ifstream &file) {
	int i, drawType;
	file.read((char *)&color, sizeof(COLORREF));
	file.read((char *)&font, sizeof(LOGFONT));
	file.read((char *)&nObject, sizeof(int));
	for (i = 0; i < nObject; i++) {
		file.read((char *)&drawType, sizeof(int));
		switch (drawType) {
		case 0:
			object[i] = new PAINTLINE();
			break;
		case 1:
			object[i] = new PAINTELLIPSE();
			break;
		case 2:
			object[i] = new PAINTRECTANGLE();
			break;
		case 3:
			object[i] = new PAINTTEXT();
			break;
		}
		object[i]->Read(file);
	}
}

void PAINTDATA::Write(ofstream &file) {
	int i;
	file.write((char *)&color, sizeof(COLORREF));
	file.write((char *)&font, sizeof(LOGFONT));
	file.write((char *)&nObject, sizeof(int));
	for (i = 0; i < nObject; i++)
		object[i]->Write(file);
	saved = TRUE;
}

PAINTOBJECT *PAINTDATA::Select(PAINTPOINT position) {
	int i;
	for (i = nObject - 1; i >= 0; i--)
		if (object[i]->Select(position))
			return object[i];
	return NULL;
}